import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';

class HomeLogin extends StatefulWidget {
  const HomeLogin({Key? key}) : super(key: key);

  @override
  State<HomeLogin> createState() => _HomeLoginState();
}

class _HomeLoginState extends State<HomeLogin> {
  final TextEditingController emailSignController = TextEditingController();
  final TextEditingController passwordSignController = TextEditingController();
  final TextEditingController confirmPasswordController =
      TextEditingController();

  // add controllers for the data to be updated and stored in the firebase cloud
  final TextEditingController nameController = TextEditingController();
  final TextEditingController surnameController = TextEditingController();
  final TextEditingController genderController = TextEditingController();

  // dispose of the email and password after usage
  @override
  void dispose() {
    emailSignController.dispose();
    passwordSignController.dispose();
    confirmPasswordController.dispose();
    // also dispose of :-
    nameController.dispose();
    surnameController.dispose();
    genderController.dispose();
    super.dispose();
  }

  // create a new user, sign up with email and password
  Future signUp() async {
    if (passwordMatchValid()) {
      await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: emailSignController.text.trim(),
        password: passwordSignController.text.trim(),
      );

      // add user personal details
      addUserDetails(nameController.text.trim(), surnameController.text.trim(),
          emailSignController.text.trim(), genderController.text.trim());
    }
  }

  // on done log in success
  final String msg = 'successful';

  Future addUserDetails(
      String name, String surname, String gender, String email) async {
    await FirebaseFirestore.instance.collection('users').add({
      'name': name,
      'surname': surname,
      'gender': gender,
      'email': email,
    }).then((value) => log(msg)).catchError((onError) => log(onError));
  }

  bool passwordMatchValid() {
    // check if password is confirmed
    if (passwordSignController.text.trim() == confirmPasswordController) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        title: const Text(
          'Firebase Sign Up',
          style: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 25,
            letterSpacing: 2.0,
          ),
        ),
        centerTitle: true,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 80,
              width: 115,
              child: Stack(),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 20.0),
              child: Text(
                'Create an Account with us Below',
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 40,
                  letterSpacing: 1.5,
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            // name controller textfield
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30.0),
              child: TextField(
                keyboardType: TextInputType.text,
                controller: nameController,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), labelText: 'First Name'),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            // surname/last name controller
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30.0),
              child: TextField(
                keyboardType: TextInputType.text,
                controller: surnameController,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), labelText: 'Surname'),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            // the gender textfield
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30.0),
              child: TextField(
                keyboardType: TextInputType.text,
                controller: genderController,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), labelText: 'Gender'),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30.0),
              child: TextField(
                keyboardType: TextInputType.emailAddress,
                controller: emailSignController,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), labelText: 'Enter email'),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            // email or username textfield
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30.0),
              child: TextField(
                keyboardType: TextInputType.visiblePassword,
                controller: passwordSignController,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), labelText: 'Enter password'),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            // confirm password for validation
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30.0),
              child: TextField(
                controller: confirmPasswordController,
                obscureText: true,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Confirm password'),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          signUp();
        },
        child: const Icon(Icons.edit_rounded),
      ),
    );
  }
}

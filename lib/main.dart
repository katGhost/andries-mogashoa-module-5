import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:log_update/pages/login_home.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:log_update/splash_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  /* StreamBuilder<User?>(
    stream: FirebaseAuth.instance.authStateChanges(),
    builder: (context, snapshot) {
      if (snapshot.hasData) {
        return LoadingPage();
      } else {
        return HomeLogin();
      }
    },
  ); */
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'login to firebase, create, save, read and update user details',
      theme: ThemeData(
        // This is the theme of your application.
        //
        primarySwatch: Colors.red,
        primaryColor: Colors.redAccent,
        backgroundColor: Colors.white,

        // text theme, all texts inherit
        textTheme: TextTheme(
          bodyText2: GoogleFonts.robotoCondensed(
            color: Colors.black87,
          ),
        ),
      ),
      home: const LoadingPage(),
    );
  }
}

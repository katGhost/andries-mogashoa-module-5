import 'package:flutter/material.dart';
import 'pages/login_home.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'dart:async';

class LoadingPage extends StatefulWidget {
  const LoadingPage({Key? key}) : super(key: key);

  @override
  State<LoadingPage> createState() => _LoadingPageState();
}

class _LoadingPageState extends State<LoadingPage> {
  @override
  void initState() {
    super.initState();
    _gotoLogin();
  }

  _gotoLogin() async {
    await Future.delayed(Duration(milliseconds: 2500), () {});
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => HomeLogin()));
  }

  // create or build the animation with flutter_spinkit_animations

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: SpinKitFadingCircle(
        itemBuilder: ((context, index) => DecoratedBox(
              decoration: BoxDecoration(
                color: index.isEven
                    ? Theme.of(context).primaryColor
                    : Theme.of(context).primaryColor,
              ),
            )),
      )),
    );
  }
}
